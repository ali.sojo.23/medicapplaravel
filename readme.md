<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Instalación en servidor

### Clonar el repositorio

<pre>
	git clone [url]
</pre>
### INSTALAR COMPOSER
<pre>
	composer install || composer update
</pre>

### Crear el archivo .env

<pre>
	cp .env.example .env
</pre>

### Modificar los datos de la base de datos por los del servidor actual

<pre>
	nano .env
</pre>
<pre>
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=laravel
	DB_USERNAME=root
	DB_PASSWORD=
</pre>

### Limpiar el la memoria caché del proyecto Uvitale

<pre>
	php artisan	optimize
</pre>

### Ejecutar la migración de la base de datos

<pre>
	php artisan migrate:fresh
</pre>

### Cargar los roles de Usuarios Basicos

<pre>
	php artisan db:seed
</pre>

### ejecutar la instalación de los passport

<pre>
	php artisan passport:install --force && php artisan passport:keys --force
</pre>