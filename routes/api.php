<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['cors']], function() {
	Route::post('registros', 'AuthController@register');
	Route::post('login', 'AuthController@login');
	Route::post('forgot/email','Auth\ForgotPasswordController@sendResetLinkEmail');
	Route::post('verify/email/{user}','EmailVerificationController@SendEmail');

	Route::group(['middleware' => ['auth:api']], function () {
		Route::apiResource('user','UserController');
		Route::apiResource('country', 'CountryController');
		Route::apiResource('city','CityController');
		Route::apiResource('education','UserEducationController');
		Route::apiResource('experiences','UserExperiencesController');
		Route::get('messages', 'MessageController@fetch');
		Route::post('smessages', 'MessageController@sentMessage');
	});

});
	
