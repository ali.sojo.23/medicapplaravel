<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
        User::insert([
            'FirstName' => 'Ali',
            'LastName'	=> 'Sojo',
            'email' 	=> 'ali@demo.local',
            'password' 	=> Hash::make('As-21103927'),
            'remember_token' => Str::random(10),
        	'email_verified_at' => now(),
        ]);
        factory(User::class, 25)->create();  
    }
}
