<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$data = array(
    		array('role_name' => 'usuario'),
    		array('role_name' => 'doctor'),
    		array('role_name' => 'administrador')
    	);

    	Role::insert($data);
    }
}
