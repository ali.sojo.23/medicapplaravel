<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_experiences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->index()->unsigned();
            $table->string('company');
            $table->bigInteger('country_id')->index()->unsigned();
            $table->bigInteger('city_id')->index()->unsigned();
            $table->string('position');
            $table->date('start');
            $table->date('end')->nullable();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users');
            $table->foreign('country_id')
                    ->references('id')
                    ->on('countries');
            $table->foreign('city_id')
                    ->references('id')
                    ->on('cities');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_experiences');
    }
}
