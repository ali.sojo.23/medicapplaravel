<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class address extends Model
{
    protected $fillable = [
    	'country_id',
    	'city_id',
    	'street'
    ];
    protected $hidden = [
    	'created_at','updated_at'
    ];

    public function ciudad(){
        return $this->hasOne('App\City', 'id', 'city_id');
    }
    public function pais(){
        return $this->hasOne('App\Country','id','country_id');
    }
}
