<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userEducation extends Model
{
    protected $fillable = [
    	'user_id',
		'institute',
		'subject',
		'start',
		'end',
		'degree',
		'grade'
    ];
}
