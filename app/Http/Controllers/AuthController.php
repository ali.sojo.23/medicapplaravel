<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function register(Request $request){

    		$validator = Validator::make($request->all(), [
        		'FirstName' => 'required',
        		'LastName' => 'required',
        		'email' => 'required|email',
        		'password' => 'required',
        		'confirm_password' => 'required|same:password',
    		]);
    		if ($validator->fails()) {
        		return response()->json(['error'=>$validator->errors()], 422);
    		}

    		$input = $request->all();

    		$input['password'] = bcrypt($request->get('password'));

            $input['FirstName'] = ucwords(strtolower($request->get('FirstName')));

            $input['LastName'] = ucwords(strtolower($request->get('LastName')));
            
    		$user = User::create($input);

    		$token =  $user->createToken('MyApp')->accessToken;

    		return response()->json([
        		'token' => $token,
        		'user' => $user
    		], 200);
	}
	public function login(Request $request){
    
    		if (Auth::attempt($request->only('email', 'password'))) {
        			$user = Auth::user();
                    $user->role;
                    $user->informacion;
        			$token =  $user->createToken('MyApp')->accessToken;
        			return response()->json([
            			'token' => $token,
            			'user'  => $user
        			], 200);
    		} else {
        	return response()->json(['error' => 'Unauthorised'], 401);
    		}
    
		}

}
