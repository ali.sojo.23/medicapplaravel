<?php

namespace App\Http\Controllers;

use App\Events\MessageSentEvent;
use Illuminate\Http\Request;
use App\message;

class MessageController extends Controller
{
    public function fetch(Request $request)
    {
    	$user = $Request->id;
        return Message::where('user_id','like',$user)->get();
    }

    public function sentMessage(Request $request)
    {
        $user = Auth::user();

        $message = Message::create([
            'message' => $request->message,
            'user_id' => Auth::user()->id,
        ]);

        broadcast(new MessageSentEvent($user, $message))->toOthers();
    }
}
