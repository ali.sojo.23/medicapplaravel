<?php

namespace App\Http\Controllers;

use App\userExperiences;
use Illuminate\Http\Request;

class UserExperiencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user_id;

        $user = userExperiences::where('user_id','=',$user)
                                    ->orderBy('start','asc')
                                    ->get();
                                    
        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->data;
        $education = userExperiences::create([
            'user_id'   => $request->id,  
            'company'   => $data['company'],
            'country_id'=> $data['country'],
            'city_id'   => $data['city'],
            'position'  => $data['position'],
            'start'     => $data['start'],
            'end'       => $data['end']
        ]);

        return response()->json([$education], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\userExperiences  $userExperiences
     * @return \Illuminate\Http\Response
     */
    public function show(userExperiences $userExperiences)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\userExperiences  $userExperiences
     * @return \Illuminate\Http\Response
     */
    public function edit(userExperiences $userExperiences)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\userExperiences  $userExperiences
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, userExperiences $userExperiences)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\userExperiences  $userExperiences
     * @return \Illuminate\Http\Response
     */
    public function destroy(userExperiences $userExperiences)
    {
        //
    }
}
