<?php

namespace App\Http\Controllers;

use App\identification;
use Illuminate\Http\Request;

class IdentificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\identification  $identification
     * @return \Illuminate\Http\Response
     */
    public function show(identification $identification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\identification  $identification
     * @return \Illuminate\Http\Response
     */
    public function edit(identification $identification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\identification  $identification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, identification $identification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\identification  $identification
     * @return \Illuminate\Http\Response
     */
    public function destroy(identification $identification)
    {
        //
    }
}
