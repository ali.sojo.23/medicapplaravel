<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Access\AuthorizationException;
use App\User;

class EmailVerificationController extends Controller
{
    public function SendEmail(User $user){
    	if (empty($user->email_verified_at)) {

            $user->sendEmailVerificationNotification();

        	return response()->json('sended', 200);
        }
    }
}
