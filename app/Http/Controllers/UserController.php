<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $user = User::all();

         return $user;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user->role;
        if($user->information_id !== null):
            $user->informacion->direccion->ciudad;
            $user->informacion->direccion->pais;
            $user->informacion->phones;
        else:
            $user->informacion;
        endif;
        return $user;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {   

        //return $request->all();
        /**************************************/
        /*                                    */
        /*Creacion y modificacion de          */
        /*Avatar de Usuario                   */
        /*                                    */
        /**************************************/
        if(!empty($request->avatar)):
            $user->avatar = $request->avatar;
            $user->save();
        
        /**************************************/
        /*                                    */
        /*Creacion y modificacion de          */
        /*Informacion de Usuario              */
        /*                                    */
        /**************************************/
        /*$info = $request->informacion;*/
        elseif($user->information_id === null):
            $info = $request->informacion;
            //creacion de la informacion
            $informacion = $user->informacion()->create([
                'birthday'   => $info['birthday'],
                'gender'     => $info['gender'],
            ]);
            //asignacion del id al usuario
            $user->information_id = $informacion->id;
            //creacion de la direccion al usuario
            $address = $user->informacion->direccion()->create([
                            'country_id' => $info['country'],
                            'city_id'    => $info['state'],
                            'street'     => $info['address']
                        ]);
            $phone = $user->informacion->phones()->create([
                            'number' => $info['phone'],
                        ]);
            //guardar direccion en la tabla de informacion y Telefono
            $user->informacion->address_id = $address->id;
            $user->informacion->phone = $phone->id;
            $user->informacion->save();
            //guardar usuario
            $user->save();
            $user->informacion;
            $user->informacion->direccion;
            $user->informacion->phone;
            return $user;
        else:
            $info = $request->informacion;
            $user->informacion->birthday    = $info['birthday'];
            $user->informacion->gender      = $info['gender'];
            if($user->informacion->address_id === null):
                    $address = $user->informacion->direccion()->create([
                            'country_id' => $info['country'],
                            'city_id'    => $info['state'],
                            'street'     => $info['address']
                        ]);
                    $user->informacion->address_id = $address->id;
            else:
                    $user->informacion->direccion->country_id = $info['country'];
                    $user->informacion->direccion->city_id    = $info['state'];
                    $user->informacion->direccion->street     = $info['address'];
                    $user->informacion->direccion->save();
            endif;
            $user->informacion->save();
            $user->informacion;
            $user->informacion->direccion;
            $user->informacion->phones;
            return $user;
        
        endif;
        /**************************************/
        /*                                    */
        /*                                    */
        /*FIN                                 */
        /*                                    */
        /**************************************/

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
