<?php

namespace App\Http\Controllers;

use App\userEducation;
use Illuminate\Http\Request;

class UserEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user_id;

        $user = userEducation::where('user_id','=',$user)
                                    ->orderBy('start','asc')
                                    ->get();

        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->data;
        $education = userEducation::create([
            'user_id'   => $request->id,  
            'institute' => $data['institute'],
            'subject'   => $data['subject'],
            'start'     => $data['start'],
            'end'       => $data['end'],
            'degree'    => $data['degree'],
            'grade'     => $data['grade']
        ]);

        return response()->json([$education], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\userEducation  $userEducation
     * @return \Illuminate\Http\Response
     */
    public function show(userEducation $userEducation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\userEducation  $userEducation
     * @return \Illuminate\Http\Response
     */
    public function edit(userEducation $userEducation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\userEducation  $userEducation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, userEducation $userEducation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\userEducation  $userEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(userEducation $userEducation)
    {
        //
    }
}
