<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class message extends Model
{
    protected $fillable = ['message', 'user_id','conversation_id'];

    public function sender()
    {
        return $this->belongsTo('App\User','id','user_id');
    }

    public function conversation()
    {
        return $this->belongsTo('App\conversation','id','conversation_id');
    }
}
