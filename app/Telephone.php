<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telephone extends Model
{
    protected $fillable = [
    	'number',
    	'verify_number'
    	];
    protected $hidden = [
    	'created_at','updated_at'
    ];
}
