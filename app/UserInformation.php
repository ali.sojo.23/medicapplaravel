<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    protected $fillable = [
    	'birthday',
    	'gender',
    	'address_id',
        'phone'
    ];
    protected $hidden = [
    	'created_at','updated_at', 'phone'
    ];

    public function direccion(){
        return $this->hasOne('App\address', 'id', 'address_id');
    }
    public function phones(){
        return $this->hasOne('App\Telephone', 'id', 'phone');
    }
}
