<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userExperiences extends Model
{
    protected $fillable =[
    	'user_id',
		'company',
		'country_id',
		'city_id',
		'position',
		'start',
		'end'
    ];
}
